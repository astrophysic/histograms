#ifndef _H1D_H_
#define _H1D_H_

#include <string>
#include <fstream>
#include <cmath>

//     [ bin[0] )[ bin[1] )[ bin[2] )[ bin[3] )[ bin[4] )
// ---|---------|---------|---------|---------|---------|---  x
//  r[0]      r[1]      r[2]      r[3]      r[4]      r[5]
//

using namespace std;

class H1D {
  int n;
  double *range;
  double *bin;
  double width;
public:
  H1D();
  H1D(int n, double a, double b);

  void Fill(double x);
  void Fill(double x, double y);

  void Print();
  void Save(string save_path);
  void Scale(double norm);
  void RadialNorm();
  double Integrate();

  double operator[](int i) const;
  double& operator[](int i);
  void operator*(double a);

  double GetMean();
  double GetRMS();
  double GetRange(int i);
  int GetBin();

};

#endif /* _H1D_H_ */
