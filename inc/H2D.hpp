#ifndef _H2D_H_
#define _H2D_H_

#include <string>
#include <cmath>

//     [ bin[0] )[ bin[1] )[ bin[2] )[ bin[3] )[ bin[4] )
// ---|---------|---------|---------|---------|---------|---  x
//  r[0]      r[1]      r[2]      r[3]      r[4]      r[5]
//

using namespace std;

class H2D {
  size_t n, m;
  double* xrange;
  double* yrange;
  double** bins;
  double xwidth;
  double ywidth;
  
public:
  H2D();
  H2D(size_t n, double x_min, double x_max, size_t m, double y_min, double y_max);
  virtual ~H2D();
  void Fill(double x, double y);
  void Fill(double x, double y, double z);

  void Save(string save_path);
  
};

#endif /* _H1D_H_ */
