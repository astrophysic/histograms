#include "H1D.hpp"

#include <iostream>

H1D::H1D(){
  //
  //Null constructor to static variable
  //
}

H1D::H1D(int n, double a, double b){
  //
  // Constructor to n bins to keepper values bettwen a and b
  //

  this->n=n;
  range = new double[n+1];
  bin = new double[n];
  width = ( b - a) / n;
  for (int i = 0; i < n; i++) {
    range[i] = a + width * i;
  }
  range[n] = b;
}

void H1D::Fill(double x){
  int pos = int( (x - range[0]) / width);
  if (pos < n && pos > -1){
    bin[pos]++;
  }
}


void H1D::Fill(double x, double y){
  int pos = int( (x - range[0]) / width);
  if (pos < n && pos > -1){
    bin[pos]+=y;
  }
}

void H1D::Print(){
  for (int i = 0; i < n; i++) {
      std::cout << "bin[" << i << "]: " << bin[i] << '\n';
  }
}

void H1D::Save(string save_path){
  ofstream out_file(save_path.c_str(),fstream::out);
  for (int i = 0; i < n; i++) {
      out_file << (range[i]+range[i+1])/2 << " " << bin[i] << '\n';
  }
  out_file.close();
}

double H1D::operator[](int i) const{
  return bin[i];
}

double& H1D::operator[](int i){
  return bin[i];
}

void H1D::operator*(double a){
  for (int i = 0; i < n; i++) {
    bin[i]*=a;
  }

}

double H1D::Integrate()
{
    double integrate=0;
    for (int i=0; i<n; i++)
    {
        integrate+=bin[i];
    }
}

double H1D::GetMean(){
  double mean=0;
  double n_entrie=0;
  for (int i = 0; i < n; i++) {
    mean+=0.5*(range[i]+range[i+1])*bin[i];
    n_entrie+=bin[i];	
  }
  return mean/n_entrie;
}
double H1D::GetRMS(){
  double mean = GetMean();
  double rms = 0;
  double n_entrie=0;
  for (int i = 0; i < n; i++) {
    rms+=bin[i]*pow(0.5*(range[i]+range[i+1])-mean,2);
    n_entrie+=bin[i];	
  }
  return sqrt(rms/n_entrie);
}

void H1D::Scale(double norm){
  for (int i = 0; i < n; i++) {
      bin[i]=bin[i]*norm;
  }
}

int H1D::GetBin(){
  return n;
}

double H1D::GetRange(int i){
  return range[i];
}

void H1D::RadialNorm()
{
  for (int i = 0; i < n; i++) 
  {
    bin[i] /= M_PI*(pow(range[i+1],2)-pow(range[i],2)); 
  }
}

