#include "H2D.hpp"

#include <iostream>
#include <fstream>
#include <iomanip>

H2D::H2D(){
  //
  //Null constructor to static variable
  //
}

H2D::H2D(size_t n, double x_min, double x_max, size_t m, double y_min, double y_max)
{
  
  //Setting number os bins
  this->n=n;
  this->m=m;

  //Allocating memory
  this->xrange = new double [n+1];
  this->yrange = new double [m+1]; 

  this->bins = new double* [n];
  for(size_t i=0; i<n; i++)
  { 
    this->bins[i] = new double[m];
  }

  for(size_t i=0; i<n; i++)
  {
    for(size_t j=0; j<m; j++)
    {
      this->bins[i][j]=0;
    }
  }


  // Difine width bin
  xwidth = ( x_max - x_min) / n;
  ywidth = ( y_max - y_min) / m;

  // Set range values
  for (size_t i = 0; i < n; i++) 
  {
    xrange[i] = x_min + xwidth * i;
  }
  for (size_t i = 0; i < m; i++) 
  {
    yrange[i] = y_min + ywidth * i;
  }

  xrange[n] = x_max;
  yrange[n] = y_max;

}

H2D::~H2D()
{
  for(size_t i=0; i<n; i++)
  {
    delete[] bins[i];
  }
  delete[] bins;
  delete[] xrange;
  delete[] yrange;
}

void H2D::Fill(double x,double y){
  int pos_x = size_t( (x - xrange[0]) / xwidth);
  int pos_y = size_t( (y - yrange[0]) / ywidth);
  if (pos_x < n && pos_x > -1)
  {
    if (pos_y < m && pos_y > -1)
    {
      bins[pos_x][pos_y]++;
    }
  }
}

void H2D::Fill(double x,double y, double z)
{
  int pos_x = size_t( (x - xrange[0]) / xwidth);
  int pos_y = size_t( (y - yrange[0]) / ywidth);

  if (pos_x < n && pos_x > -1 && pos_y < m && pos_y > -1)
  {
    bins[pos_x][pos_y]+=z;
  }
}


void H2D::Save(string save_path){
  ofstream out_file(save_path.c_str(),fstream::out);
   out_file << std::fixed;
  for (size_t i = 0; i < n; i++) 
  {
    for (size_t j = 0; j < m; j++) 
    {
      out_file << std::setw(15) << std::setprecision(6) << (xrange[i]+xrange[i+1])/2 << " "
               << std::setw(15) << std::setprecision(6) << (yrange[j]+yrange[j+1])/2 << " "
               << std::setw(15) << std::setprecision(6) << bins[i][j] << '\n';
    }
    out_file << '\n';
  }
  out_file.close();
}
